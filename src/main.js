import Cursor from './entities/cursor'
import Pad from './entities/pad'
import CONFIG from './config'
import { gameLoop } from './utils'

const cursor = new Cursor()
cursor.get()

const pad = new Pad()

const canvas = document.getElementById('board')
const ctx = canvas.getContext('2d')
const cursorSprite = new Image()
cursorSprite.src = '/assets/smile.png'

const run = () => {
	ctx.fillStyle = 'green'
	ctx.fillRect(0, 0, CONFIG.board.dimensions.x, CONFIG.board.dimensions.y)
	const { x, y } = cursor.get()
	ctx.drawImage(cursorSprite, x, y)

	cursor.move('x', pad.getAxeX() * 30)
	cursor.move('y', pad.getAxeY() * 30)
}

gameLoop(run)

const moveLeftBtn = document.getElementById('move_left')
moveLeftBtn.addEventListener('click', () => {
	cursor.move('x', -10)
})
