export function gameLoop(functionToCall, args) {
	functionToCall(args)
	window.requestAnimationFrame(() => gameLoop(functionToCall, args))
}
