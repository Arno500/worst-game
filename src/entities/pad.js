import { gameLoop } from '../utils'

export default class Pad {
	constructor() {
		this.ready = false
		this.controller = null
		addEventListener('gamepadconnected', () => this.connectHandler())
	}

	connectHandler() {
		this.ready = true
		gameLoop(
			() =>
				(this.controller = Array.from(navigator.getGamepads()).find(
					elm => elm.mapping === 'standard'
				))
		)
	}

	getAxeX() {
		if (this.ready) {
			return this.controller.axes[0]
		}
		return 0
	}
	getAxeY() {
		if (this.ready) {
			return this.controller.axes[1]
		}
		return 0
	}
}
