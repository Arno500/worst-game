import CONFIG from '../config'

export default class Cursor {
	constructor() {
		// append later
		this.position = {
			x: Math.floor(
				CONFIG.board.dimensions.x / 4 +
					(Math.random() * CONFIG.board.dimensions.x * 1) / 2
			),
			y: Math.floor(
				CONFIG.board.dimensions.y / 4 +
					(Math.random() * CONFIG.board.dimensions.y * 1) / 2
			)
		}
	}

	// direction: 'x' | 'y'
	move(direction, distance) {
		this.position[direction] += distance
	}

	get() {
		return this.position
	}
}
